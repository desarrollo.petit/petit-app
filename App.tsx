import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {Button, LogBox, View} from 'react-native';
import {Provider} from 'react-redux';
import i18n from './languages/i18n';
import {StackNavigator} from './src/navigation/NavigationStack';
import {store} from './src/redux/store';
import {I18nextProvider} from 'react-i18next';

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
]);

const App = () => {
  return (
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <View style={{flexDirection: 'row'}}>
          <View style={{width: '50%'}}>
            <Button
              color="#841584"
              title="idioma inglés"
              onPress={() => i18n.changeLanguage('en')}
            />
          </View>
          <View style={{width: '50%'}}>
            <Button
              color="#D990F3"
              title="idioma español"
              onPress={() => i18n.changeLanguage('es')}
            />
          </View>
        </View>
        <NavigationContainer>
          <StackNavigator />
        </NavigationContainer>
      </Provider>
    </I18nextProvider>
  );
};
export default App;
