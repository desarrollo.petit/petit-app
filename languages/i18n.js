import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import english from './english.json';
import spanish from './spanish.json';

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  lng: 'es',
  resources: {
    en: english,
    es: spanish,
  },
  react: {
    useSuspense: false,
  },
});

export default i18n;
