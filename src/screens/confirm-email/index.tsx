import React from 'react';
import {ActivityIndicator, Button, Text} from 'react-native';
import {auth} from '../../../firebase';
import {useAuthentication} from '../../hooks/useAuthentication';
import fbauth from '@react-native-firebase/auth';

export const ConfirmEmail = () => {
  // const [msg, setMsg] = useState<string>('');
  // const [loading, setLoading] = useState(false);
  const {sendNewEmailVerification, msg, loadingSendEmail} = useAuthentication();
  console.log('renderizando confirmEmail');

  return (
    <>
      <Text>
        Debes confirmar su dirección de correo. Por favor, revise su correo y la
        bandeja de SPAM. Podemos enviarle otro correo electrónico de
        confirmación haciendo click en el siguiente botón
      </Text>
      {loadingSendEmail ? (
        <>
          <ActivityIndicator size="large" color="#00ff00" />
        </>
      ) : (
        <Text style={{color: 'red'}}>{msg}</Text>
      )}
      <Button
        title="Enviar nuevo email"
        onPress={() => {
          // setLoading(true);
          // sendEmailVerification(auth.currentUser!)
          //   .then(() => {
          //     setLoading(false);
          //     setMsg('email enviado desde vista confirmacion email');
          //   })
          //   .catch(err => {
          //     if (err.code === 'auth/too-many-requests') {
          //       setMsg(
          //         'Ya se ha enviado un correo de confirmación, por favor, prueba más tarde, demasiadas peticiones',
          //       );
          //     }
          //     setLoading(false);
          //   });
          sendNewEmailVerification();
        }}
      />
      <Text>
        Puedes iniciar sesión, una vez hayas verificado tu correo electrónico
      </Text>
      <Button
        title="Ir a inicio de sesión"
        onPress={() => fbauth().signOut()}
      />
    </>
  );
};
