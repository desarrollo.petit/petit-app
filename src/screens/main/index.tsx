import React, {useRef} from 'react';
import {Button, Text, useWindowDimensions, View} from 'react-native';
import {auth} from '../../../firebase';
import { AppHeader } from '../../components/AppHeader';
import { SearchBar } from '../../components/SearchBar';

import Carousel from 'react-native-snap-carousel';
import { Publication } from '../../components/Publication/Publication';
import { FABMenu } from '../../components/FAB/FABMenu';
import { FABSubMenuButton } from '../../components/FAB/FABSubMenuButton';

// MOCK DATA

const imagenes2 = [
  {
    user: 'ELMEJORpug',
    image:
      'https://estaticos-cdn.prensaiberica.es/clip/a5ee7a2a-6f63-4ab9-8986-ba83113aca56_16-9-discover-aspect-ratio_default_0.jpg',
    description:
      'Ya ha llegado la navidad, cada año Dami se vuelve loca con el árbol. Cuando lo estoy poniendo se come la mitad de las bolas y lo deja todo perdido',
    location: 'Madrid, España',
    time: 'Hace 2 horas',
    comments: 38,
    likes: 1347,
    profileImage:
      'http://c.files.bbci.co.uk/48DD/production/_107435681_perro1.jpg',
  },
  {
    user: 'ELMEJORhuskieblanco',
    image:
      'https://i.pinimg.com/564x/fe/55/0a/fe550a7c75a9457b6f5072349da74d2e--white-siberian-husky-siberian-huskies.jpg',
    description: 'Aquí, en la nieve',
    location: 'Siberia, Rusia',
    time: 'Hace 30 minutos',
    comments: 380,
    likes: 13470,
    profileImage:
      'https://www.marvelousdogs.com/wp-content/uploads/2021/06/White-Husky.jpg',
  },
  {
    user: 'ELMEJORhuskiegris',
    image:
      'https://www.mundoperro.net/wp-content/uploads/cachorro-husky-siberiano-gris.jpg',
    description: 'Aullando a la luna, mirando al océano',
    location: 'Siberia, Rusia',
    time: 'Hace 30 minutos',
    comments: 380,
    likes: 13470,
    profileImage:
      'https://p4.wallpaperbetter.com/wallpaper/509/768/6/husky-puppy-face-grass-wallpaper-preview.jpg',
  },
  {
    user: 'ELMEJORgerman',
    image: 'https://www.terranea.es/assets/images/razas/pastor_aleman2.jpg',
    description: 'Entrenando para hacer la San Silvestre en tiempo record',
    location: 'Madrid, España',
    time: 'Hace 2 horas',
    comments: 38,
    likes: 1347,
    profileImage: 'https://estag.fimagenes.com/img/4/6/m/W/6mW_900.jpg',
  },
  {
    user: 'ELMEJORbeagle',
    image:
      'https://www.feelcats.com/wp-content/uploads/2019/08/beagle-caracteristicas.jpg',
    description: 'Custodiando el bosque',
    location: 'Siberia, Rusia',
    time: 'Hace 30 minutos',
    comments: 380,
    likes: 13470,
    profileImage:
      'https://i.pinimg.com/736x/88/26/36/88263690123c7d76f0cd0fd6a33e5f58.jpg',
  },
];

// PROPORCIONES (%)

// Header (Lun 12   Petit   Campana)
const headerSize = 5;

// SearchBar ( Amigos Todos    Lupa)
const  searchBarSize = 4;

// Publication
const publicationsSize = 91

export const MainScreen = ({navigation}: any) => {
  let activeSlide = useRef(0);
  const lengthSlide = imagenes2.length;
  let {height} = useWindowDimensions();
  height = height * 0.91;
  return (
    <View style={{flex: 1}}>
    <View>
      <Text>MainScreen</Text>
      <View style={{position: 'absolute'}}>
        <Button title="CERRAR SESION" onPress={() => auth.signOut()} />
      </View>
    </View>
      <View style={{flex: headerSize}}>
        <AppHeader />
      </View>
      <View style={{flex: searchBarSize}}>
        <SearchBar />
      </View>
      <View style={{flex: publicationsSize}}>
        <Carousel
          data={imagenes2}
          renderItem={({item}: any) => <Publication {...item} />}
          vertical={true}
          sliderHeight={height}
          itemHeight={height}
          onSnapToItem={index => {
            activeSlide.current = index;
          }}
          onScrollEndDrag={event => {
            const {contentSize, contentOffset} = event.nativeEvent;
            // console.log("------------------------")
            // console.log("activeSlide.current", activeSlide.current)
            // console.log("lengthSlide",lengthSlide)
            // console.log("contentOffset.y",contentOffset.y)
            // console.log("contentSize.height",contentSize.height)
            //Detect when swipe right at the last item
            if (
              activeSlide.current === lengthSlide - 1 &&
              contentOffset.y >=
                contentSize.height * ((lengthSlide - 1) / lengthSlide)
            ) {
              console.log('carga mas');
            } else if (
                activeSlide.current === 0 &&
                contentOffset.y === 0
            ) {
                console.log("busca nuevos")
            }
          }}
        />
      </View>
      <FABMenu positionament="bc" backgroundColor='orange' borderRadius={100} width={70} height={70}>
        <FABSubMenuButton
          backgroundColor="orange"
          height={40}
          top={-40}
          width={40}
          left={-70}
          onPress={() => navigation.navigate('ChatScreen')}>
          <Text style={{fontSize: 20, color: 'white'}}>1</Text>
        </FABSubMenuButton>
        <FABSubMenuButton
          backgroundColor="orange"
          height={40}
          top={-60}
          width={40}
          left={-20}
          onPress={() => navigation.navigate('EventsScreen')}>
          <Text style={{fontSize: 20, color: 'white'}}>2</Text>
        </FABSubMenuButton>
        <FABSubMenuButton
          backgroundColor="orange"
          height={40}
          top={-40}
          width={40}
          left={30}
          onPress={() => navigation.navigate('PersonScreen')}>
          <Text style={{fontSize: 20, color: 'white'}}>3</Text>
        </FABSubMenuButton>
      </FABMenu>
    </View>
  );
};
