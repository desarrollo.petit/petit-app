import {Formik} from 'formik';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {ActivityIndicator, Button, Text, View} from 'react-native';
import {auth} from '../../../firebase';
import {CustomTextInput} from '../../components/CustomTextInput';
import {useSignIn} from '../../hooks/useSignIn';

export const ForgotPassword = ({navigation}) => {
  const {sendPasswordByEmail, loading, msg} = useSignIn();
  const {t} = useTranslation();
  return (
    <>
      <Formik
        initialValues={{userOrEmail: ''}}
        onSubmit={values => {
          sendPasswordByEmail(values.userOrEmail);
        }}
        // validationSchema={validationForgotPasswordForm(t)}
      >
        {({handleSubmit}) => (
          <>
            <View>
              <Text>Recuperar contraseña</Text>
              {loading ? (
                <>
                  <ActivityIndicator size="large" color="#00ff00" />
                </>
              ) : (
                <Text style={{color: 'red'}}>{msg}</Text>
              )}
              <CustomTextInput
                label={'userOrEmail'}
                placeholder={t('userOrEmail')}
                name="userOrEmail"
              />
            </View>
            <View>
              <Button onPress={handleSubmit} title={'Recuperar contraseña'} />
              <Button
                onPress={() => navigation.navigate('Login')}
                title={'Ir a inicio de sesión'}
              />
            </View>
          </>
        )}
      </Formik>
    </>
  );
};
