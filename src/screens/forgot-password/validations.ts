import {TFunction} from 'react-i18next';
import * as Yup from 'yup';
export const validationForgotPasswordForm = (
  t: TFunction<'translation', undefined>,
) =>
  Yup.object({
    email: Yup.string().email(t('InvalidFormat')).required(t('Required')),
  });
