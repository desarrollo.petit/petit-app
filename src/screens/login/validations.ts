import {TFunction} from 'react-i18next';
import * as Yup from 'yup';
export const validationLoginForm = (t: TFunction<'translation', undefined>) =>
  Yup.object({
    email: Yup.string().email(t('InvalidFormat')).required(t('Required')),
    password: Yup.string().min(6, t('MinimunPassword')).required(t('Required')),
  });
