import {Formik} from 'formik';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {ActivityIndicator, Button, Text, View} from 'react-native';
import {CustomTextInput} from '../../components/CustomTextInput';
import {useSignIn} from '../../hooks/useSignIn';
import {validationLoginForm} from './validations';

export const FormLogin = ({navigation}) => {
  const {signInUserWithEmailAndPassword, loading, msg, signInWithGoogle} =
    useSignIn();
  const {t} = useTranslation();
  return (
    <>
      <Formik
        initialValues={{email: '', password: ''}}
        onSubmit={values => {
          signInUserWithEmailAndPassword(values.email, values.password);
        }}
        validationSchema={validationLoginForm(t)}>
        {({handleSubmit}) => (
          <>
            <View>
              <Text>{t('Login')}</Text>
              {loading ? (
                <>
                  <ActivityIndicator size="large" color="#00ff00" />
                </>
              ) : (
                <Text>{msg}</Text>
              )}
              <CustomTextInput
                label={t('Email')}
                placeholder={t('Email')}
                name="email"
              />
              <CustomTextInput
                label={t('Password')}
                placeholder={t('Password')}
                name="password"
              />
            </View>
            <View>
              <Button onPress={handleSubmit} title={t('Login')} />
              <Button
                title={t('SingUp')}
                onPress={() => navigation.navigate('Register')}
              />
              <Button
                title={t('ForgotPassword')}
                onPress={() => navigation.navigate('ForgotPassword')}
              />
            </View>
          </>
        )}
      </Formik>
      <Button onPress={signInWithGoogle} title="sign in with google" />
    </>
  );
};
