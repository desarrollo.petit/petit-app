import React from 'react';
import {ScrollView, View} from 'react-native';
import {FormLogin} from './FormLogin';

export const Login = ({navigation}) => {
  return <FormLogin navigation={navigation} />;
};
