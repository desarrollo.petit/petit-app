import React from 'react';
import {Button, Text} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import {auth} from '../../firebase';
import {State} from '../redux/reducers';
import {useTranslation} from 'react-i18next';

export const TESTSCREEN = () => {
  const {userReducer} = useSelector((state: State) => state);
  const {t} = useTranslation();
  console.log(userReducer.uid);
  return (
    <ScrollView>
      <Text>TESTSCREEN</Text>
      <Text>{JSON.stringify(userReducer.uid)}</Text>
      <Text>{t('HelloPablo')}</Text>
      <Button title={t('Logout')} onPress={() => auth.signOut()} />
    </ScrollView>
  );
};
