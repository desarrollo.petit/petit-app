import {TFunction} from 'react-i18next';
import * as Yup from 'yup';

export const validationRegisterForm = (
  t: TFunction<'translation', undefined>,
) =>
  Yup.object({
    username: Yup.string().required(t('Required')),
    email: Yup.string().email(t('InvalidFormat')).required(t('Required')),
    password: Yup.string().min(6, t('MinimunPassword')).required(t('Required')),
    password2: Yup.string()
      .oneOf([Yup.ref('password'), null], t('SamePasswords'))
      .required(t('Required')),
    recaptcha: Yup.boolean().oneOf([true], t('RecaptchaVerification')),
  });
