import {Formik} from 'formik';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {ActivityIndicator, ScrollView, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import CustomButton from '../../../components/CustomButton';
import CustomInput from '../../../components/CustomInput';
import CustomPasswordInput from '../../../components/CustomPasswordInput';
import {RecaptchaCheckbox} from '../../../components/RecaptchaCheckbox';
import {useCreateUser} from '../../../hooks/useCreateUser';
import {validationRegisterForm} from '../validations';
import styles from './styles';

const checkIcon = (
  <Icon
    name="check"
    size={30}
    color="green"
    style={{position: 'absolute', right: 50, top: 20}}
  />
);

const FormRegister = ({navigation}) => {
  const {createWithEmailAndPassword, loading, msg} = useCreateUser();
  const {t} = useTranslation();
  return (
    <Formik
      initialValues={{
        username: '',
        email: '',
        password: '',
        password2: '',
        recaptcha: false,
      }}
      onSubmit={values => {
        createWithEmailAndPassword(
          values.username,
          values.email,
          values.password,
        );
      }}
      validationSchema={validationRegisterForm(t)}>
      {({handleSubmit}) => (
        <>
          <ScrollView>
            <Text style={styles.title}>{t('SingUp')}</Text>
            {loading ? (
              <>
                <ActivityIndicator size="large" color="#00ff00" />
              </>
            ) : (
              <Text>{msg}</Text>
            )}
            <CustomInput
              placeholder={t('Username')}
              name="username"
              label={t('Username')}
            />
            <CustomInput
              placeholder={t('Email')}
              name="email"
              label={t('Email')}
              validate={true}
              icon={checkIcon}
            />
            <CustomPasswordInput
              name="password"
              icon={true}
              placeholder={t('Password')}
              label={t('Password')}
            />
            <CustomPasswordInput
              name="password2"
              icon={true}
              placeholder={t('RepeatPassword')}
              label={t('RepeatPassword')}
            />
            <RecaptchaCheckbox name="recaptcha" />
          </ScrollView>
          <CustomButton text={t('CheckIn')} onPress={handleSubmit} />
          <Text style={{marginTop: 15, alignSelf: 'center'}}>
            {t('HaveAccount')}
          </Text>
          <CustomButton
            text={t('Login')}
            onPress={() => navigation.navigate('Login')}
          />
        </>
      )}
    </Formik>
  );
};

export default FormRegister;
