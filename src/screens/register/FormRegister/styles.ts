import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
});

export default styles;
