import React from 'react';
import {Text, View} from 'react-native';
import FormRegister from './FormRegister';
import styles from './styles';

export const Register = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Petit</Text>
      <FormRegister navigation={navigation} />
    </View>
  );
};
