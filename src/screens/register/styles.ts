import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FF9680', //#ff6834
  },
  title: {
    fontSize: 50,
    color: 'white',
    fontWeight: '500',
    textAlign: 'center',
    marginBottom: 15,
    marginTop: 25,
  },
});

export default styles;
