import fbauth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {DocumentData} from 'firebase/firestore';
import {useState} from 'react';
import {useTranslation} from 'react-i18next';

GoogleSignin.configure({
  webClientId:
    '783794459653-tfutcce3du8phtf1vjpu1as626sqmu65.apps.googleusercontent.com',
});

export const useSignIn = () => {
  const [loading, setLoading] = useState(false);
  const [msg, setMsg] = useState('');
  const {t} = useTranslation();
  const signInUserWithEmailAndPassword = async (
    email: string,
    passwordValue: string,
  ) => {
    setLoading(true);
    fbauth()
      .signInWithEmailAndPassword(email, passwordValue)
      .then(user => {
        console.log(user);
        console.log(user.user.emailVerified);
        if (!user.user.emailVerified) {
          console.log('no has verificado tu email');
          //navegar a pantalla de volver a pedir
        }
        // setMsg(`Ha iniciado sesión correctamente ${email}`);
        // setLoading(false);
      })
      .catch(err => {
        console.log(err);
        if (
          err.code === 'auth/wrong-password' ||
          err.code === 'auth/user-not-found'
        ) {
          setMsg(t('IncorrectAccount'));
        }
        if (err.code === 'auth/too-many-requests') {
          setMsg(t('AccountBloqued'));
        }
        setLoading(false);
      });
  };

  const signInWithGoogle = async () => {
    const {idToken} = await GoogleSignin.signIn();
    // Create a Google credential with the token
    const googleCredential = fbauth.GoogleAuthProvider.credential(idToken);
    // Sign-in the user with the credential
    return fbauth().signInWithCredential(googleCredential);
  };

  const sendPasswordByEmail = async (userOrEmail: string) => {
    setLoading(true);
    const userCollection = firestore().collection('users');
    let user: DocumentData | undefined;
    let recipientEmail = userOrEmail;
    try {
      if (userOrEmail.includes('@')) {
        user = await userCollection.where('email', '==', userOrEmail).get();
        user = user.docs[0];
        recipientEmail = userOrEmail;
      } else {
        user = await userCollection.doc(userOrEmail).get();
        recipientEmail = user.data().email;
      }
    } catch (err) {
      setMsg('El nombre de usuario o correo electrónico no son correctos');
    }

    if (user && user.exists) {
      fbauth()
        .sendPasswordResetEmail(recipientEmail)
        .then(() => {
          setMsg('email enviado correctamente');
          setLoading(false);
        })
        .catch(err => {
          console.log(err);
          if (
            err.code === 'auth/user-not-found' ||
            err.code === 'auth/invalid-email'
          ) {
            setMsg(
              'El nombre de usuario o correo electrónico no son correctos',
            );
          }
          setLoading(false);
        });
    } else {
      setMsg('El nombre de usuario o correo electrónico no son correctos');
      setLoading(false);
    }
  };

  return {
    signInUserWithEmailAndPassword,
    sendPasswordByEmail,
    loading,
    msg,
    signInWithGoogle,
  };
};
