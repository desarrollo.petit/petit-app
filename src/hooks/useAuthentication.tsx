import fbauth from '@react-native-firebase/auth';
import {useMemo, useState} from 'react';
import {useDispatch} from 'react-redux';
import {updateUser} from '../redux/user/actionCreator';

export const useAuthentication = () => {
  const [loading, setLoading] = useState(true);
  const [loadingSendEmail, setLoadingSendEmail] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const [userAuth, setUserAuth] = useState<any>({});
  const dispatch = useDispatch();
  // const getUserAuth = () =>
  //   auth.onAuthStateChanged(user => {
  //     setLoading(false);
  //     setUserAuth(user);
  //     dispatch(updateUser(user));
  //   });
  const GetUserAuth = () =>
    useMemo(() => {
      fbauth().onAuthStateChanged(user => {
        console.log('---------memooo-----');
        setLoading(false);
        setUserAuth(user);
        dispatch(updateUser(user));
      });
    }, []);

  const sendNewEmailVerification = async () => {
    setLoadingSendEmail(true);
    await fbauth()
      .currentUser?.sendEmailVerification()
      .then(() => {
        setLoadingSendEmail(false);
        setMsg('email enviado desde vista confirmacion email');
      })
      .catch(err => {
        if (err.code === 'auth/too-many-requests') {
          setMsg(
            'Ya se ha enviado un correo de confirmación, por favor, prueba más tarde, demasiadas peticiones',
          );
        }
        setLoadingSendEmail(false);
      });
  };

  return {
    userAuth,
    loading,
    GetUserAuth,
    sendNewEmailVerification,
    msg,
    loadingSendEmail,
  };
};
