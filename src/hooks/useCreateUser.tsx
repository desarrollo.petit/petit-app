import fbauth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {useState} from 'react';
import {useTranslation} from 'react-i18next';

export const useCreateUser = () => {
  const [loading, setLoading] = useState(false);
  const [msg, setMsg] = useState('');
  const {t} = useTranslation();
  const createWithEmailAndPassword = async (
    username: string,
    email: string,
    passwordValue: string,
  ) => {
    setLoading(true);
    const userCollection = firestore().collection('users');
    // const user = await getDoc(doc(db, 'users', username));
    const user = await userCollection.doc(username).get();
    if (user.exists) {
      setMsg(t('UserNameExists', {myVar: username}));
      setLoading(false);
      return;
    }
    await fbauth()
      .createUserWithEmailAndPassword(email, passwordValue)
      .then(async userCredential => {
        console.log(userCredential);
        // const authentication = getAuth();
        fbauth()
          .currentUser?.sendEmailVerification()
          .then(() => console.log('email enviado desde creación de usuario'));
        // setMsg(`${email} registrado correctamente`);
        // setLoading(false);
        // await setDoc(doc(db, 'users', username), {
        await userCollection.doc(username).set({
          email,
          // fullname: userData.fullname,
          // keyword: generateUsernameKeywords(userData.username),
          // phone: userData.phone,
          username,
          birthday: new Date(),
          bio: '',
          // gender: 2,
          // followings: [userData.username],
          requestedList: [],
          searchRecent: [],
          storyNotificationList: [],
          postNotificationList: [],
          website: '',
          avatarURL: 'https:direccion/avatar.prueba',
          // privacySetting: {
          //   ...defaultUserState.setting?.privacy,
          // },
          // notificationSetting: {
          //   ...defaultUserState.setting?.notification,
          // },
        });
      })
      .catch(err => {
        if (err.code === 'auth/email-already-in-use') {
          setMsg(t('EmailExists', {myVar: email}));
          setLoading(false);
          console.log(err);
        }
      });
  };

  return {createWithEmailAndPassword, loading, msg};
};
