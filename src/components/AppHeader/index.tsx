import React from 'react';
import {Text, View} from 'react-native';

export const AppHeader = () => {
  return (
    <View style={{flexDirection:'row', justifyContent:'space-between', paddingHorizontal:10, alignItems:'center'}}>
      <Text style={{flex:1, textAlign:'left', fontSize:15 }}>Lun 12</Text>
      <Text style={{flex:1, textAlign:'center', fontSize:30 }}>Petit</Text>
      <Text style={{flex:1, textAlign:'right', fontSize:15}}>Campana</Text>
    </View>
  );
};
