import React from 'react';
import {Image, Text, View} from 'react-native';

export const SearchBar = () => {
  return (
    <View>
      <View
        style={{
          borderTopWidth: 1,
          borderBottomWidth: 1,
          borderColor: 'lightgrey',
          paddingVertical: 5,
          flexDirection: 'row',
          justifyContent: 'center',
        }}>
        <Text style={{marginHorizontal: 10}}>Amigos</Text>
        <Text style={{marginHorizontal: 10}}>Todos</Text>
        <Text style={{position: 'absolute', right: 10, alignSelf: 'center'}}>
          Lupa
        </Text>
      </View>
    </View>
  );
};
