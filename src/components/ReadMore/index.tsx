import React, {useCallback, useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

interface Props {
  text: string;
  readMorePosition: 'left' | 'center' | 'right';
  readLessPosition: 'left' | 'center' | 'right';
}
export const ReadMore = ({text, readMorePosition, readLessPosition}: Props) => {
  const [loadMore, setLoadMore] = useState(false);
  const [numOfLinesText, setNumOfLinesText] = useState(0);

  const onTextLayout = useCallback(e => {
    if (numOfLinesText == 0) setNumOfLinesText(e.nativeEvent.lines.length);
  }, []);

  const numberOfLines = loadMore ? 0 : 1;

  return (
    <View>
      <Text numberOfLines={numberOfLines} onTextLayout={onTextLayout}>
        {text}
      </Text>
      {!loadMore && numOfLinesText > 1 ? (
        <TouchableOpacity
          onPress={() => {
            setLoadMore(true);
          }}>
          <Text style={{textAlign: readMorePosition}}>Leer más</Text>
        </TouchableOpacity>
      ) : (
        loadMore && (
          <TouchableOpacity
            onPress={() => {
              setLoadMore(false);
            }}>
            <Text style={{textAlign: readLessPosition}}>Leer menos</Text>
          </TouchableOpacity>
        )
      )}
    </View>
  );
};
