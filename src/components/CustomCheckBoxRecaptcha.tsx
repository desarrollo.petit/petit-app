import CheckBox from '@react-native-community/checkbox';
import {ErrorMessage, useField} from 'formik';
import React, {useEffect} from 'react';
import {Text} from 'react-native';

//NO SE USA PERO SIRVE DE EJEMPLO PARA CREAR EL FUTURO CHECKBOX AL USO REUTILIZABLE
export const CustomCheckBoxRecaptcha = (props: any) => {
  const {name, value} = props;
  const [, , helpers] = useField(props);
  const {setValue} = helpers;
  useEffect(() => {
    setValue(value);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);
  return (
    <>
      <CheckBox {...props} />
      <Text style={{fontSize: 12, color: '#FF0D10'}}>
        <ErrorMessage name={name} />
      </Text>
    </>
  );
};
