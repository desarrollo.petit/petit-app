import React from 'react';
import {Text, TextInput} from 'react-native';
import {ErrorMessage, useField} from 'formik';

interface Props {
  label: string;
  name: string;
  placeholder?: string;
  [x: string]: any;
}

export const CustomTextInput = ({...props}: Props) => {
  const [field, , helpers] = useField(props);
  return (
    <>
      <TextInput
        {...props}
        value={field.value}
        // onBlur={() => helpers.setTouched(!meta.touched)}
        onBlur={() => helpers.setTouched(true)}
        onChangeText={helpers.setValue}
      />
      <Text style={{fontSize: 12, color: '#FF0D10'}}>
        <ErrorMessage name={props.name} />
      </Text>
    </>
  );
};
