import {ErrorMessage, useField} from 'formik';
import React, {useState} from 'react';
import {Text, TextInput, View} from 'react-native';
import styles from '../GeneralInput.styles';

interface Props {
  name: string;
  label: string;
  placeholder?: string;
  icon?: Element;
  validate?: boolean;
}

const CustomInput = ({icon, validate = false, ...props}: Props) => {
  const [field, meta, helpers] = useField(props);
  const [isFocused, setIsFocused] = useState(false);

  const handleOnBlur = () => {
    // helpers.setTouched(!meta.touched);
    helpers.setTouched(true);
    setIsFocused(false);
  };

  return (
    <View>
      <TextInput
        {...props}
        value={field.value}
        onFocus={() => setIsFocused(true)}
        onBlur={handleOnBlur}
        onChangeText={helpers.setValue}
        style={[styles.input, isFocused ? styles.colorFocus : styles.colorBlur]}
      />
      {validate
        ? !meta.error && icon !== null && field.value.length !== 0
          ? icon
          : null
        : icon}
      <Text style={{fontSize: 12, color: 'black', marginLeft: 40}}>
        <ErrorMessage name={props.name} />
      </Text>
    </View>
  );
};

export default CustomInput;
