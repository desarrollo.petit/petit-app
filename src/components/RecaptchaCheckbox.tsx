import CheckBox from '@react-native-community/checkbox';
import {ErrorMessage, useField} from 'formik';
import React, {useRef, useState} from 'react';
import {Image, Switch, Text, TouchableOpacity, View} from 'react-native';
import Recaptcha from 'react-native-recaptcha-that-works';
import i18n from '../../languages/i18n';

interface Props {
  name: string;
}
export const RecaptchaCheckbox = (props: Props) => {
  const {name} = props;
  const [, , helpers] = useField(props);
  const {setValue} = helpers;
  const [valueRecaptcha, setValueRecaptcha] = useState(false);
  const recaptcha = useRef<any>();
  const openReCaptcha = () => {
    recaptcha.current.open();
  };

  const onVerify = (token: string) => {
    console.log('success!', token);
    setValueRecaptcha(true);
    setValue(true);
  };

  const onExpire = () => {
    console.warn('expired!');
  };
  return (
    <>
      <TouchableOpacity onPress={openReCaptcha}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <Image
            style={{
              width: 75,
              height: 75,
            }}
            source={require('../assets/img/recaptcha.png')}
          />
        </View>
      </TouchableOpacity>
      <CheckBox value={valueRecaptcha} />
      <Switch value={valueRecaptcha} />
      <Text style={{fontSize: 12, color: '#FF0D10'}}>
        <ErrorMessage name={name} />
      </Text>
      <Recaptcha
        ref={recaptcha}
        siteKey="6LeArJ8eAAAAAHak95zjwlYKDyeVngoRk-JF6Ny2"
        baseUrl="https://www.google.com/recaptcha/admin/site/513780864/setup"
        onVerify={onVerify}
        onExpire={onExpire}
        lang={i18n.language}
      />
    </>
  );
};
