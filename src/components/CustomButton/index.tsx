import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles';

interface Props {
  text: string;
  onPress?: (e: React.FormEvent<HTMLFormElement>) => void;
  disabled?: boolean;
}

const CustomButton = ({text, disabled = false, ...props}: Props) => {
  return (
    <TouchableOpacity
      {...props}
      disabled={disabled}
      style={[styles.container, disabled && styles.containerDisabled]}
      activeOpacity={0.75}>
      <Text style={[styles.text, disabled && styles.textDisabled]}>{text}</Text>
    </TouchableOpacity>
  );
};

export default CustomButton;
