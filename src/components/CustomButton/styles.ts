import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    backgroundColor: '#f7f7f7',
    minWidth: 150,
    paddingVertical: 10,
    borderRadius: 15,
    marginVertical: 5,
  },
  text: {
    color: '#ff6834',
    fontSize: 16,
    alignSelf: 'center',
  },
  containerDisabled: {
    backgroundColor: '#e3dede',
  },
  textDisabled: {
    color: '#ff8d66',
  },
});

export default styles;
