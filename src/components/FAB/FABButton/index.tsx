import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'

type Position = 'tl' | 'tc' | 'tr' | 'cl' | 'cc' | 'cr' | 'bl' | 'bc' | 'br';

export const FABButton = ({action, styles}:any) => {
  return (
    <View
        style={{
        //   justifyContent: 'center',
        //   alignItems: 'center',
          backgroundColor:'red'
        }}>
        <TouchableOpacity
          style={styles} onPress={action}>
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Text style={{fontSize: 40, color: 'white'}}>H</Text>
          </View>
        </TouchableOpacity>
      </View>
  )
}
