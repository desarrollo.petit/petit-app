import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';

interface Props {
  backgroundColor: string;
  width: string | number;
  height: string | number;
  top: string | number;
  children: any;
  left?: string | number;
  onPress():void 
}

export const FABSubMenuButton = ({
  backgroundColor,
  width,
  height,
  top,
  left,
  children,
  onPress
}: Props) => {
  return (
    <TouchableOpacity
      style={{
        backgroundColor: backgroundColor,
        borderRadius: 100,
        width,
        height,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: top,
        left,
        position:'absolute'
      }}
      onPress={onPress}>
      <View style={{flexDirection: 'row', justifyContent: 'center'}}>
        {children}
      </View>
    </TouchableOpacity>
  );
};
