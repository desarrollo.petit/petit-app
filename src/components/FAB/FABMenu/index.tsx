import React, {useState} from 'react';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {styles} from './styles';

type Position = 'tl' | 'tc' | 'tr' | 'cl' | 'cc' | 'cr' | 'bl' | 'bc' | 'br';

interface Props {
  children: any;
  positionament: Position;
  backgroundColor: string;
  borderRadius: number;
  width: number;
  height: number;
}

export const FABMenu = ({
  children,
  positionament,
  backgroundColor,
  borderRadius,
  width,
  height,
}: Props) => {
  const [showOptions, setShowOptions] = useState(false);
  console.log(positionament);

  const positionStyles = getPosition(positionament);

  const subMenuPosition =
    positionament === 'tc' ? styles.subMenuBottom : styles.subMenuTop;

  const changeShowOptions = () => {
    setShowOptions(!showOptions);
  };
  return (
    <View
      style={{
        // flex: 1,
        position: 'absolute',
        ...positionStyles,
      }}>
      <View>
        {showOptions && (
          <View style={{...styles.subMenu, ...subMenuPosition}}>
            {children}
          </View>
        )}
        <View>
          <TouchableOpacity
            style={{
              backgroundColor,
              borderRadius,
              width,
              height,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={changeShowOptions}>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <Text style={{fontSize: 40, color: 'white'}}>H</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const getPosition = (position: Position) => {
  switch (position) {
    case 'tl':
      return {...styles.leftHorizontal, ...styles.topVertical};
    case 'tc':
      return {...styles.centerHorizontal, ...styles.topVertical};
    case 'tr':
      return {...styles.rightHorizontal, ...styles.topVertical};
    case 'cl':
      return {...styles.leftHorizontal, ...styles.centerVertical};
    case 'cc':
      return {...styles.centerHorizontal, ...styles.centerVertical};
    case 'cr':
      return {...styles.rightHorizontal, ...styles.centerVertical};
    case 'bl':
      return {...styles.leftHorizontal, ...styles.bottomVertical};
    case 'bc':
      return {...styles.centerHorizontal, ...styles.bottomVertical};
    case 'br':
      return {...styles.rightHorizontal, ...styles.bottomVertical};
  }
};
