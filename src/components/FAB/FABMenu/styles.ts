import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    centerHorizontal: {
      alignSelf: 'center',
    },
    centerVertical: {
      alignItems: 'center',
    },
    leftHorizontal: {
      left: 10,
    },
    topVertical: {
      justifyContent: 'flex-start',
    },
    rightHorizontal: {
      right: 10,
    },
    bottomVertical: {
      bottom: 10,
    },
    subMenu: {
      flexDirection: 'row',
      justifyContent: 'center',
      position: 'absolute',
      alignSelf: 'center',
    },
    subMenuTop: {
      bottom: 60,
    },
    subMenuBottom: {
      top: 60,
    },
  });