import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  input: {
    borderStyle: 'solid',
    borderWidth: 2,
    borderRadius: 10,
    fontSize: 16,
    textAlign: 'center',
    paddingHorizontal: 10,
    paddingRight: 40,
    marginHorizontal: 40,
    marginVertical: 10,
    color: '#ebebeb',
  },
  colorFocus: {
    borderColor: '#ff6834',
  },
  colorBlur: {
    borderColor: 'white',
  },
});

export default styles;
