import React from 'react';
import {Image, Text, View} from 'react-native';
import { ReadMore } from '../ReadMore';

interface Props {
  user: string;
  description: string;
  profileImage: string;
}

export const PublicationDetail = ({user, description, profileImage}: Props) => {
  return (
    <View
      style={{
        //   flex:10,
        padding: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
       backgroundColor: 'red',
        position: 'absolute',
        // height:500,
        // width:'100%'

      }}>
      <View style={{flex: 1, flexDirection: 'row'}}>
        <View
          style={{
          // backgroundColor: 'pink',
          }}>
          <Image
            style={{
              width: 50,
              height: 50,
              borderRadius: 100,
              //backgroundColor: 'pink',
            }}
            source={{
              uri: `${profileImage}`,
            }}
          />
        </View>
        <View
          style={{
            //flexWrap: 'wrap',
            //overflow:'hidden',
          // backgroundColor: 'yellow',
            flex: 1,
            padding: 5,
          }}>
          <Text style={{fontWeight: 'bold'}}> @{user}</Text>
          <ReadMore text={description} readMorePosition='right' readLessPosition='right' />
        </View>
        <View>
          <Text>Puntitos</Text>
        </View>
      </View>
    </View>
  );
};
