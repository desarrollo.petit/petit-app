import React from 'react';
import {Image, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {PublicationDetail} from './PublicationDetail';
// import ReadMore from 'react-native-read-more-text';

interface Props {
  user: string;
  image: string;
  description: string;
  location: string;
  time: string;
  comments: number;
  likes: number;
  profileImage: string;
}

export const Publication = ({
  user,
  image,
  description,
  location,
  time,
  comments,
  likes,
  profileImage,
}: Props) => {
  return (
    <View style={{height: '100%'}}>
      <View style={{flex: 10}} />
      <View
        style={{
          padding: 5,
          flex: 70,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
            paddingHorizontal: 5,
          }}>
          <Text>{location}</Text>
          <Text>{time}</Text>
        </View>
        <Image
          style={{
            flex: 1,
          }}
          source={{
            uri: `${image}`,
          }}
        />
      </View>
      <View
        style={{
          flex: 10,
          justifyContent: 'center',
          borderBottomWidth: 1,
          borderColor: 'lightgrey',
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Text style={{flex: 1, textAlign: 'center'}}>
            {comments} comentarios
          </Text>
          <Text style={{flex: 1, textAlign: 'center'}}>{likes} Me gusta</Text>
          <Text style={{flex: 1, textAlign: 'center'}}>
            Guardar en tu perfil
          </Text>
        </View>
      </View>
      <View
        style={{
          padding: 5,
          flex: 10,
        }}></View>
      <PublicationDetail
        description={description}
        profileImage={profileImage}
        user={user}
      />
    </View>
  );
};
