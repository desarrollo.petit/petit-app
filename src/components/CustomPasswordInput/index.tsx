import React, {useState} from 'react';
import {Text, TextInput, View} from 'react-native';
import styles from '../GeneralInput.styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {ErrorMessage, useField} from 'formik';

interface Props {
  name: string;
  label: string;
  placeholder?: string;
  icon?: boolean;
}

const CustomPasswordInput = ({icon = false, ...props}: Props) => {
  const [isFocused, setIsFocused] = useState(false);
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [field, , helpers] = useField(props);

  const handleOnBlur = () => {
    // helpers.setTouched(!meta.touched);
    helpers.setTouched(true);
    setIsFocused(false);
  };

  const eyeOn = (
    <Icon
      onPress={() => setIsPasswordVisible(!isPasswordVisible)}
      name="eye"
      size={30}
      style={{position: 'absolute', right: 50, top: 20}}
    />
  );

  const eyeOff = (
    <Icon
      onPress={() => setIsPasswordVisible(!isPasswordVisible)}
      name="eye-off"
      size={30}
      style={{position: 'absolute', right: 50, top: 20}}
    />
  );

  return (
    <View>
      <TextInput
        {...props}
        secureTextEntry={!isPasswordVisible}
        value={field.value}
        onFocus={() => setIsFocused(true)}
        onBlur={handleOnBlur}
        onChangeText={helpers.setValue}
        style={[styles.input, isFocused ? styles.colorFocus : styles.colorBlur]}
      />
      {icon && (isPasswordVisible ? eyeOn : eyeOff)}
      <Text style={{fontSize: 12, color: 'black', marginLeft: 40}}>
        <ErrorMessage name={props.name} />
      </Text>
    </View>
  );
};

export default CustomPasswordInput;
