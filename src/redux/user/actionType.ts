export enum ActionTypeUpdateUser {
  UPDATE_USER = 'UPDATE_USER',
  LOGIN = 'LOGIN',
  LOGOUT = 'LOGOUT',
}
