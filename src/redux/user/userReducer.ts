import {IActionUser} from './actions';
import {ActionTypeUpdateUser} from './actionType';

const initialState: any = {};
export const userReducer = (state = initialState, action: IActionUser) => {
  switch (action.type) {
    case ActionTypeUpdateUser.UPDATE_USER:
      return {...state, ...action.payload};
    case ActionTypeUpdateUser.LOGIN:
      return {...state, ...action.payload};
    case ActionTypeUpdateUser.LOGOUT:
      return {...state, ...action.payload};
    default:
      return state;
  }
};
