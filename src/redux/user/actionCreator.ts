import {Dispatch} from 'react';
import {ActionTypeUpdateUser} from './actionType';

export const updateUser = (user: any) => {
  //llamada a bbdd
  const bbddRequest = {
    publicaciones: ['perro', 'gato', 'conejo'],
    amigos: ['Antonio', 'Mario', 'Vicente'],
  };
  return (dispatch: Dispatch<any>) => {
    dispatch({
      type: ActionTypeUpdateUser.UPDATE_USER,
      payload: {...user, ...bbddRequest},
    });
  };
};
export const login = (user: any) => {
  return (dispatch: Dispatch<any>) => {
    dispatch({
      type: ActionTypeUpdateUser.LOGIN,
      payload: user,
    });
  };
};

export const logout = () => {
  return (dispatch: Dispatch<any>) => {
    dispatch({
      type: ActionTypeUpdateUser.LOGOUT,
    });
  };
};
