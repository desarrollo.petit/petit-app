import {ActionTypeUpdateUser} from './actionType';
interface UpdateUser {
  type: ActionTypeUpdateUser.UPDATE_USER;
  payload: object;
}
interface Login {
  type: ActionTypeUpdateUser.LOGIN;
  payload: object;
}
interface Logout {
  type: ActionTypeUpdateUser.LOGOUT;
  payload: object;
}

export type IActionUser = UpdateUser | Login | Logout;
