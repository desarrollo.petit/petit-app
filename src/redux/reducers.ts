import React from 'react'
import { combineReducers } from 'redux'
import { userReducer } from './user/userReducer';

export const reducers = combineReducers({
    userReducer 
})


export type State = ReturnType<typeof reducers>;