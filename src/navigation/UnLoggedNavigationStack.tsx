import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Login} from '../screens/login';
import {Register} from '../screens/register';
import {ForgotPassword} from '../screens/forgot-password';

const Stack = createStackNavigator();

export const UnLoggedNavigationStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{title: 'Regístrate'}}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{title: 'ForgotPassword'}}
      />
    </Stack.Navigator>
  );
};
