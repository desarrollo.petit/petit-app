import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {ChatScreen} from '../screens/ChatScreen';
import {EventsScreen} from '../screens/EventsScreen';
import {MainScreen} from '../screens/main';
import {PersonScreen} from '../screens/PersonScreen';

const Stack = createStackNavigator();

export const LoggedNavigationStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MainScreen"
        component={MainScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ChatScreen"
        component={ChatScreen}
        options={{title: 'Esta es la del chat'}}
      />
      <Stack.Screen
        name="EventsScreen"
        component={EventsScreen}
        options={{title: 'Esta es la de los eventos'}}
      />
      <Stack.Screen
        name="PersonScreen"
        component={PersonScreen}
        options={{title: 'Esta es la de la persona'}}
      />
    </Stack.Navigator>
  );
};
