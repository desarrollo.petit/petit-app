import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {ConfirmEmail} from '../screens/confirm-email';

const Stack = createStackNavigator();

export const ConfirmEmailNavigationStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="ConfirmEmail"
        component={ConfirmEmail}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};
