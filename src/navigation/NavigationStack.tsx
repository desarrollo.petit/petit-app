import React from 'react';
import {ActivityIndicator, Text} from 'react-native';
import {useAuthentication} from '../hooks/useAuthentication';
import {ConfirmEmailNavigationStack} from './ConfirmEmailNavigationStack';
import {LoggedNavigationStack} from './LoggedNavigationStack';
import {UnLoggedNavigationStack} from './UnLoggedNavigationStack';

export const StackNavigator = () => {
  const {loading, userAuth, GetUserAuth} = useAuthentication();

  GetUserAuth();
  console.log('renderizando stack');
  if (loading) {
    return (
      <>
        <Text>Cargando...</Text>
        <ActivityIndicator size="large" color="#00ff00" />
      </>
    );
  }
  return (
    <>
      {userAuth && userAuth.emailVerified && <LoggedNavigationStack />}
      {userAuth && !userAuth.emailVerified && <ConfirmEmailNavigationStack />}
      {!userAuth && <UnLoggedNavigationStack />}
    </>
  );
};
